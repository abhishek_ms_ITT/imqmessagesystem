Project Name
IMQ

ConsoleApplication
This project was generated with .NET CLI version 3.1

Restore the Dependencies
Run dotnet restore to restore dependencies as well as project-specific tools that are specified in the project file.

Build
Run dotnet build to build the project. The dotnet build command builds the project and its dependencies into a set of binaries. The binaries include the project's code in Intermediate Language (IL) files with a .dll extension.

Run
Execute dotnet run to run the project.

Running unit tests
Run dotnet test to execute the unit tests via XUNIT.