using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Client
{
    class ClientController
    {
        private TcpClient client;
        private bool isLoggedIn = false;
        private ClientService clientService;
        private PublisherService publisherService;
        private SubscriberService subscriberService;
        private NetworkHandler networkHandler = new NetworkHandler();
        private string userName;
        private string userRole;
        private string password;

        public ClientController()
        {
            client = networkHandler.intiliazeClientNetwork();
            networkHandler.createStreams();
            clientService = new ClientService(networkHandler);
            publisherService = new PublisherService(networkHandler);
            subscriberService = new SubscriberService(networkHandler);
        }

        public void connectToServer()
        {
            try
            {
                Console.WriteLine("Welcome to IMQ!!!");
            login:
                this.readUserInputs();
                isLoggedIn = clientService.sendLoginRequest(userName, userRole, password);
                if (isLoggedIn)
                {
                    while (true)
                    {
                        if (userRole.ToUpper() == ApplicationConstants.publisher.ToUpper())
                        {
                            showPublisherOptions();
                        }
                        else if (userRole.ToUpper() == ApplicationConstants.subsciber.ToUpper())
                        {
                            showSubscriberOptions();
                        }
                    }
                }
                else
                {
                    int userChoice = 0;
                    Console.WriteLine("\nInvalid Credentials!! Please select Below Options\n");
                    do
                    {
                        try
                        {
                            Console.WriteLine("1.SignIn Again \n 2.SignUp\n");
                            userChoice = int.Parse(Console.ReadLine());
                            switch (userChoice)
                            {
                                case 1:
                                    goto login;
                                case 2:
                                    clientService.addClientToSystem();
                                    goto login;
                                default:
                                    Console.WriteLine("Please select valid option.");
                                    break;
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Invalid Input");
                        }
                    } while (userChoice != 3);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
            }

        }

        private void readUserInputs()
        {
            int role = 0;
            Console.WriteLine("Please Login with your credentials:\n");
            Console.WriteLine("Pleas enter User name: ");
            userName = Console.ReadLine();
            Console.WriteLine("Please enter password:");
            password = Console.ReadLine();
            try
            {
                Console.WriteLine("Select your role \n 1.Publisher \n 2.Subscriber\n");
                role = int.Parse(Console.ReadLine());
                if (!(role == 1 || role == 2))
                {
                    Console.WriteLine("Enterd role is not valid:\n");
                    readUserInputs();
                }
            }
            catch
            {
                Console.WriteLine("Invalid input");
            }
            userRole = role == 1 ? ApplicationConstants.publisher : ApplicationConstants.subsciber;
        }

        private void showPublisherOptions()
        {
            int userChoice;
            this.showAllTopics();
            try
            {
                Console.WriteLine("\n\nSelect Below Options: \n 1.PublishMessage \n 2.Create Topic");
                userChoice = int.Parse(Console.ReadLine());
                switch (userChoice)
                {
                    case 1:
                        publisherService.publishMessage();
                        break;
                    case 2:
                        publisherService.createTopic();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                Console.WriteLine("Invalid input");
            }
        }

        private void showSubscriberOptions()
        {
            int options;
            this.showAllTopics();
            this.showTopicsWhichUserSubscribed();
            try
            {
                Console.WriteLine("\n\nSelect Below Options : \n 1.PullMessages \n 2.Subscribe to Topic");
                options = int.Parse(Console.ReadLine());
                switch (options)
                {
                    case 1:
                        this.showPulledMessages();
                        break;
                    case 2:
                        subscriberService.subscribeTopic(userName);
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                Console.WriteLine("Invalid Input");
            }
        }

        private void showAllTopics()
        {
            Console.WriteLine("\nAvailable Topics:");
            List<string> topics = clientService.getAllTopics();
            foreach (var topic in topics)
            {
                Console.WriteLine("{0}", topic);
            }
        }

        private void showTopicsWhichUserSubscribed()
        {
            Console.WriteLine("\nTopics you subscribed:");
            List<string> topicsSubscribed = subscriberService.getTopicsWhichUserSubscribed(userName);
            foreach (var topic in topicsSubscribed)
            {
                Console.WriteLine("{0}", topic);
            }
        }

        private void showPulledMessages()
        {
            List<string> pulledMessages = subscriberService.pullMessage(userName);
            Console.WriteLine("\nPulled Messages:");
            foreach (var message in pulledMessages)
            {
                Console.WriteLine("{0}", message);
            }
        }
    }
}