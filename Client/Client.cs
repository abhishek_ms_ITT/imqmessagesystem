﻿using System;

namespace Client
{
    public class IMQClient
    {
        static public void Main(string[] Args)
        {
            ClientController clientController = new ClientController();
            try
            {
                clientController.connectToServer();
            }
            catch
            {
                Console.WriteLine("Unable to connect to server now!!");
            }
        }
    }
}