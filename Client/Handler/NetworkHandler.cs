using System;
using System.IO;
using System.Net.Sockets;
using IMQ.Protocol;
using Newtonsoft.Json;
using IMQ.Exceptions;

namespace Client
{
    class NetworkHandler
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        LogHandler logger = new LogHandler();

        public TcpClient intiliazeClientNetwork()
        {
            
            try
            {
                client = new TcpClient(ApplicationConstants.ipAddress, ApplicationConstants.portNumber);
                logger.Info("Connected to server."); 
                return client;
            }
            catch
            {
                logger.Error("Failed to connect to server!! Please try again later"); 
                return null;
            }
        }

        public void createStreams()
        {
            stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }

        public void closeConnection()
        {
            stream.Close();
            client.Close();
            client.Dispose();
        }

        public Response read()
        {
            Console.WriteLine(streamReader);
            Response response = JsonConvert.DeserializeObject<Response>(streamReader.ReadLine());
            if (response.header == ApplicationConstants.IMQHeader)
            {
                return response;
            }
            throw new Exception("Invalid Request");
        }

        public void write(Request request)
        {
            try
            {
                streamWriter.WriteLine(JsonConvert.SerializeObject(request));
                streamWriter.Flush();
            }
            catch
            {
                throw new NetworkStreamException("There is some problem with the server. Please try again later");
            }
        }
    }
}