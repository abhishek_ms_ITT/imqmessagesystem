using System;
using System.Collections.Generic;
using IMQ.Protocol;

namespace Client
{
    class ClientService
    {
        NetworkHandler networkHandler;
        public ClientService(NetworkHandler networkHandler)
        {
            this.networkHandler = networkHandler;
        }
        
        public List<string> getAllTopics()
        {
            sendMessageForGettingAllTopics();
            List<string> receivedTopics = receiveTopicsFromServer();
            return receivedTopics;
        }

        public void sendMessageForGettingAllTopics()
        {
            Request request = new Request();
            request.action = ApplicationConstants.getAllTopic;
            networkHandler.write(request);
        }

        public List<string> receiveTopicsFromServer()
        {
            Response response = networkHandler.read();
            List<string> receivedtopics = response.data;
            return receivedtopics;
        }

        public bool sendLoginRequest(string clientName, string role, string password)
        {
            Request loginRequest = new Request();
            loginRequest.clientName = clientName;
            loginRequest.role = role;
            loginRequest.password = password;
            loginRequest.action = ApplicationConstants.login;
            networkHandler.write(loginRequest);
            bool islogin = receiveLoginResponse();
            Console.WriteLine(islogin);
            return islogin;
        }

        private bool receiveLoginResponse()
        {
            Response response = networkHandler.read();
            string loginSuccess = response.responseMessage;
            if (loginSuccess == "true")
            {
                Console.WriteLine("Login Successful\n");
                return true;
            }
            else
            {
                Console.WriteLine("Login Failed\n");
                return false;
            }
        }

        public void addClientToSystem()
        {
            int role = 0;
            string userRole = "";
            Console.WriteLine("Register:");
            Console.WriteLine("Please Enter Name:");
            var clientName = Console.ReadLine();
            Console.WriteLine("Please Create Password:");
            var password = Console.ReadLine();
            Console.WriteLine("Select your role \n 1. Publisher \n 2.Subscriber \n Press any Key to Quit\n");
            role = int.Parse(Console.ReadLine());
            if (!(role == 1 || role == 2))
            {
                Console.WriteLine("Enterd role is not valid:\n");
                addClientToSystem();
            }
            userRole = role == 1 ? ApplicationConstants.publisher : ApplicationConstants.subsciber;
            Request request = new Request();
            request.clientName = clientName;
            request.role = userRole;
            request.password = password;
            request.action = ApplicationConstants.register;
            networkHandler.write(request);
        }
    }
}