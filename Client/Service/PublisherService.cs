using System;
using IMQ.Protocol;

namespace Client
{
    class PublisherService
    {
        NetworkHandler networkHandler;
        public PublisherService(NetworkHandler networkHandler)
        {
            this.networkHandler = networkHandler;
        }

        public void publishMessage()
        {
            string topic = getTopicName();
            string message = getMessage();
            sendMessage(topic, message);
            receiveResponseFromServer();
        }

        public void sendMessage(string topic, string message)
        {
            Request request = new Request();
            request.action = ApplicationConstants.publishMessage;
            request.message = message;
            request.topic = topic;
            this.networkHandler.write(request);
        }

        public void createTopic()
        {
            string topic = getTopicName();
            sendRequestCreateTopic(topic);
            receiveResponseFromServer();
        }

        private void sendRequestCreateTopic(string topic)
        {
            Request request = new Request();
            request.action = ApplicationConstants.createTopic;
            request.topic = topic;
            this.networkHandler.write(request);
        }

        private string getMessage()
        {
            Console.WriteLine("Enter the message:");
            string messageToSend = Console.ReadLine();
            return messageToSend;
        }

        private string getTopicName()
        {
            Console.WriteLine("Enter Topic Name:");
            string topic = Console.ReadLine();
            return topic;
        }

        private void receiveResponseFromServer()
        {
            Response response = this.networkHandler.read();
            string receivedMessage = response.responseMessage;
            Console.WriteLine("\nResponse From Server: {0}\n", receivedMessage);
        }
    }
}