using System;
using System.Collections.Generic;
using IMQ.Protocol;

namespace Client
{
    class SubscriberService
    {
        NetworkHandler networkHandler;
        public SubscriberService(NetworkHandler networkHandler)
        {
            this.networkHandler = networkHandler;
        }
        public List<string> receiveTopicsFromServer()
        {
            Response response = networkHandler.read();
            List<string> receivedtopics = response.data;
            return receivedtopics;
        }

        public List<string> getTopicsWhichUserSubscribed(string clientName1)
        {
            sendRequestForGettingSubscribedTopics(clientName1);
            List<string> receivedTopics = receiveTopicsFromServer();
            return receivedTopics;
        }

        public void sendRequestForGettingSubscribedTopics(string clientName1)
        {
            Request request = new Request();
            request.action = ApplicationConstants.getSubscribedTopic;
            request.clientName = clientName1;
            networkHandler.write(request);
        }

        public List<string> pullMessage(string clientName1)
        {
            string topic = getTopic();
            sendMessagesForUserSelectedTopic(topic, clientName1);
            List<string> receivedMessages = getMessagesWhichUserSubscribed();
            return receivedMessages;
        }

        public void sendMessagesForUserSelectedTopic(string topic, string clientName1)
        {
            Request request = new Request();
            request.action = ApplicationConstants.pullMessages;
            request.clientName = clientName1;
            request.topic = topic;
            networkHandler.write(request);
        }

        public List<string> getMessagesWhichUserSubscribed()
        {
            Response response = networkHandler.read();
            List<string> receivedMessages = response.data;
            return receivedMessages;
        }

        public void subscribeTopic(string clientName1)
        {
            string topic = getTopic();
            sendMessageToSubscribe(topic, clientName1);
            Response response = networkHandler.read();
            string receivedMessage = response.responseMessage;
            Console.WriteLine("\nResponse From Server: {0}\n", receivedMessage);
        }

        public void sendMessageToSubscribe(string topic, string clientName1)
        {
            Request request = new Request();
            request.action = ApplicationConstants.subscribeToTopic;
            request.topic = topic;
            request.clientName = clientName1;
            networkHandler.write(request);
        }

        private string getTopic()
        {
            Console.WriteLine("Enter the Topic Name:");
            string topic = Console.ReadLine();
            return topic;
        }
    }
}