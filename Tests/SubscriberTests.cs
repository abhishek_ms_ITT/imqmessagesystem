using Xunit;
using Server.Services;
using Moq;
using IMQ.Protocol;
using IMQ.Tests;
using Server.Contracts;

namespace IMQ.Subscriber.Tests
{
    public class SubscriberTests
    {
        [Fact]
        public void getSubscribedTopics_shouldReturnSubscribedTopics_shouldPass()
        {
            var mockRepo = new Mock<IDatbaseHandler>();
            var request = new Request();
            request.clientName = "ram";
            mockRepo.Setup(x => x.getSubscribedTopics(request.clientName))
                .Returns(TestData.getSubscribedTopics());
            var subService = new SubscriberService(mockRepo.Object);
            var actual = subService.getSubscribedTopics(request.clientName);
            var expected = TestData.getSubscribedTopics();
            Assert.True(actual != null);
            mockRepo.Verify(x => x.getSubscribedTopics(request.clientName), Times.Exactly(1));
            Assert.Equal(actual.Count, expected.Count);
        }

        [Fact]
        public void pullSubscribedMessages_shouldPass()
        {
            var mockRepo = new Mock<IDatbaseHandler>();
            var request = new Request();
            request.clientName = "ram";
            request.topic = "subject";
            mockRepo.Setup(x => x.pullMessages(request.topic, request.clientName))
            .Returns(TestData.getAllSubscriberMessages());
            var subService = new SubscriberService(mockRepo.Object);
            var actual = subService.pullMessages(request.topic, request.clientName);
            var expected = TestData.getAllSubscriberMessages();
            Assert.True(actual != null);
            mockRepo.Verify(x => x.pullMessages(request.topic, request.clientName), Times.Exactly(1));
            Assert.Equal(actual.Count, expected.Count);
        }
    }
}