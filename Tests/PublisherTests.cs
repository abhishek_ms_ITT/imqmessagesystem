using Xunit;
using Server.Services;
using Moq;
using IMQ.Protocol;
using IMQ.Tests;
using Server.Contracts;

namespace IMQ.Publisher.Tests
{
    public class PublisherTests
    {
        [Fact]
        public void publishMessage_shouldCallRepo_shouldPass()
        {
            var mockRepo = new Mock<IDatbaseHandler>();
            var request = new Request();
            request.topic = "subject";
            request.message = "math";
            mockRepo.Setup(x => x.savePublisherMessage(request.topic, request.message));
            var pubService = new PublisherService(mockRepo.Object);
            pubService.saveMessageInDB(request.topic, request.message);
            mockRepo.Verify(x => x.savePublisherMessage(request.topic, request.message), Times.Exactly(1));

        }

        [Fact]
        public void getAllTopics_ReturnsTopicList_shouldPass()
        {
            var mockRepo = new Mock<IDatbaseHandler>();
            mockRepo.Setup(x => x.getAllTopics())
            .Returns(TestData.getAllTopics());
            var clientService = new ClientService(mockRepo.Object);
            var request = new Request();
            request.topic = "subject";
            var actual = clientService.getAllTopics();
            var expected = TestData.getAllTopics();
            Assert.True(actual != null);
            mockRepo.Verify(x => x.getAllTopics(), Times.Exactly(1));
            Assert.Equal(actual.Count, expected.Count);
        }

        [Fact]
        public void createTopic_shouldCallRepo_shouldPass()
        {
            var mockRepo = new Mock<IDatbaseHandler>();
            var req = new Request();
            req.topic = "subject";
            mockRepo.Setup(x => x.createTopic(req.topic));
            var pubService = new PublisherService(mockRepo.Object);
            pubService.insertTopic(req.topic);
            mockRepo.Verify(x => x.createTopic(req.topic), Times.Exactly(1));
        }
    }
}
