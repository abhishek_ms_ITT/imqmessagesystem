using System.Collections.Generic;
using Server.Models;

namespace IMQ.Tests
{
    public static class TestData
    {

        public static List<string> getAllTopics()
        {

            var mockData = new List<string>();
            var msg1 = "topic1";
            var msg2 = "topic2";
            mockData.Add(msg1);
            mockData.Add(msg2);
            return mockData;
        }
        public static List<string> getSubscribedTopics()
        {
            var mockData = new List<string>();
            var msg1 = "topic1";
            var msg2 = "topic2";
            mockData.Add(msg1);
            mockData.Add(msg2);
            return mockData;
        }

        public static List<string> getAllSubscriberMessages()
        {
            var mockData = new List<string>();
            var msg1 = "topic1msg1";
            var msg2 = "topic1msg2";
            var msg3 = "topic1msg3";

            mockData.Add(msg1);
            mockData.Add(msg2);
            mockData.Add(msg3);
            return mockData;
        }
    }
}