using Xunit;
using Server.Services;
using Moq;
using IMQ.Protocol;
using Server.Contracts;

namespace IMQ.Login.Tests
{
    public class LoginTests
    {

        [Fact]
        public void verifyClient_shouldVerifyPublisher_shouldPass()
        {
            var dbhandler = new Mock<IDatbaseHandler>();
            var request = new Request();
            request.role = "publisher";
            request.clientName = "ram";
            request.password = "abc";
            dbhandler.Setup(x => x.verifyClientCredentials(request.clientName, request.role, request.password));
            var clientService = new ClientService(dbhandler.Object);
            clientService.verfiyClientCredentials(request.clientName, request.role, request.password);
            dbhandler.Verify(x => x.verifyClientCredentials(request.clientName, request.role, request.password), Times.Exactly(1));
        }

        [Fact]
        public void verifyClient_shouldVerifySubscriber_shouldPass()
        {
            var dbhandler = new Mock<IDatbaseHandler>();
            var request = new Request();
            request.role = "subscriber";
            request.clientName = "raj";
            request.password = "abc";
            var clientService = new ClientService(dbhandler.Object);
            clientService.verfiyClientCredentials(request.clientName, request.role, request.password);
            dbhandler.Verify(x => x.verifyClientCredentials(request.clientName, request.role, request.password), Times.Exactly(1));
        }
    }
}
