namespace Server.Models
{
    public class ExpiredMessage
    {
        public int topicId {get; set;}
        public string message {get; set;}
    }
}