using System;
using System.Net;
using System.Net.Sockets;
using Server.Contracts;
using System.Threading;
using Client;
using IMQ.Exceptions;

namespace Server.Services
{
    class ServerService : IServerService
    {
        private IClientService clientService;
        private IClientRequestController requestController;
        TcpClient client;
        LogHandler logger = new LogHandler();
        public ServerService(IClientService clientService, IClientRequestController requestController)
        {
            this.clientService = clientService;
            this.requestController = requestController;
        }
        public void startServer()
        {
            String ipAddress = ApplicationConstants.ipAddress;
            IPAddress localAddress = IPAddress.Parse(ipAddress);
            TcpListener server = new TcpListener(localAddress, ApplicationConstants.portNumber);
            int clientCount = 0;
            try
            {
                server.Start();
                Console.WriteLine("Server is Running!!!");
                logger.Info("Server is Running!!!");
                while (true)
                {
                    clientCount += 1;
                    client = server.AcceptTcpClient();
                    Thread newThread = new Thread(handleClientRequest);
                    newThread.Start();
                }
            }
            catch
            {
                throw new ServerStartException("Unable to start the server");
            }
        }

        public void handleClientRequest()
        {
            requestController.handleClientRequest(client);
        }
    }
}
