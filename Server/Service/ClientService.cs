using System.Collections.Generic;
using Server.Contracts;
using IMQ.Protocol;

namespace Server.Services
{
    public class ClientService : IClientService
    {
        IDatbaseHandler dbhandler;

        public ClientService(IDatbaseHandler datbaseHandler)
        {
            this.dbhandler = datbaseHandler;
        }
        
        public void sendAllTopics(NetworkHandler networkHandler, Request request)
        {
            Response response = new Response();
            List<string> topics = getAllTopics();
            response.data = topics;
            networkHandler.write(response);
        }

        public List<string> getAllTopics()
        {
            List<string> topics = dbhandler.getAllTopics();
            return topics;
        }
        
        public void createUser(NetworkHandler networkHandler, Request request)
        {  
            Response response = new Response();
            string role = request.role;
            string clientName = request.clientName;
            string password = request.password;
            dbhandler.createUser(clientName, role, password);
            response.responseMessage = "Registered SucessFully";
            networkHandler.write(response);
        }

        public void authenticateClient(NetworkHandler networkHandler, Request newLoginRequest)
        {
            bool isValidUser = verifyClient(newLoginRequest);
            sendLoginResponse(networkHandler, newLoginRequest.clientName, isValidUser);
        }

        private bool verifyClient(Request request)
        {
            bool isValidUser = false;
            string clientName = request.clientName;
            string role = request.role;
            string password = request.password;
            isValidUser = verfiyClientCredentials(clientName, role, password);
            return isValidUser;
        }
        
        public bool verfiyClientCredentials(string clientName, string role, string password)
        {
            bool isValidUser = dbhandler.verifyClientCredentials(clientName, role, password);
            return isValidUser;
        }

        public void sendLoginResponse(NetworkHandler networkHandler, string clientName, bool isValidUser)
        {
            Response response = new Response();
            response.responseMessage = isValidUser ? "true" : "false";
            networkHandler.write(response);
        }
    }
}
