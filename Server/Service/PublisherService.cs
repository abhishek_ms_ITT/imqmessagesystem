using Server.Contracts;
using IMQ.Protocol;

namespace Server.Services
{
    public class PublisherService : IPublisherService
    {        
        IDatbaseHandler dbhandler;

        public PublisherService(IDatbaseHandler datbaseHandler)
        {
            this.dbhandler = datbaseHandler;
        }
        
        public void publishMessage(NetworkHandler networkHandler, Request request)
        {
            string topic = request.topic;
            string clientMessage = request.message;
            saveMessageInDB(topic, clientMessage);
            dbhandler.savePublisherMessage(topic, clientMessage);
            Response response = new Response();
            response.responseMessage = "Published Message To Topic";
            networkHandler.write(response);
        }

        public void saveMessageInDB(string topic, string clientMessage)
        {
            dbhandler.savePublisherMessage(topic, clientMessage);
        }

         public void createTopic(NetworkHandler networkHandler, Request request)
        {
            string topicName = request.topic;
            insertTopic(topicName);
            Response response = new Response();
            response.responseMessage = "Created Topic";
            networkHandler.write(response);
        }

        public void insertTopic(string topicName)
        {
            dbhandler.createTopic(topicName);
        }
    }
}