using System.Collections.Generic;
using Server.Contracts;
using IMQ.Protocol;

namespace Server.Services
{
    public class SubscriberService : ISubscriberService
    {
        IDatbaseHandler dbhandler;
        public SubscriberService(IDatbaseHandler datbaseHandler)
        {
            this.dbhandler = datbaseHandler;
        }

        public void subscribeTopic(NetworkHandler networkHandler, Request request)
        {
            string topicName = request.topic;
            string clientName = request.clientName;
            dbhandler.subscribeToTopic(clientName, topicName);
            Response response = new Response();
            response.responseMessage = "Subscribed To Topic";
            networkHandler.write(response);
        }

        public void getSubscribedTopics(NetworkHandler networkHandler, Request request)
        {
            string clientName = request.clientName;
            List<string> topics = getSubscribedTopics(clientName);
            Response response = new Response();
            response.data = topics;
            networkHandler.write(response);
        }

        public List<string> getSubscribedTopics(string clientName)
        {
            List<string> topics = dbhandler.getSubscribedTopics(clientName);
            return topics;
        }
        public void pullMessages(NetworkHandler networkHandler, Request request)
        {
            string topicName = request.topic;
            string client = request.clientName;
            List<string> messages = pullMessages(topicName, client);
            Response response = new Response();
            response.data = messages;
            networkHandler.write(response);
        }

        public List<string> pullMessages(string topicName, string clientName)
        {
            List<string> messages = dbhandler.pullMessages(topicName, clientName);
            return messages;
        }
    }
}