using System;
using System.IO;
using Client;

namespace Server.Services
{
    public class FileOperations
    {
        LogHandler logger = new LogHandler();
        public void WriteDataToFile(string data, string fileName)
        {
            string outputDirectory = createDirectory();
            fileName = fileName + ".txt";
            fileName = Path.Combine(outputDirectory, fileName);
            data = DateTime.Now.ToString() + " : " + data;

            try
            {
                using (StreamWriter writer = new StreamWriter(fileName, true))
                {
                    writer.WriteLine(data);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
                logger.Error(Ex.ToString()); 
            }
        }

        public string createDirectory()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string outputDirectory = Path.Combine(currentDirectory, "Output");

            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            return outputDirectory;
        }
    }
}