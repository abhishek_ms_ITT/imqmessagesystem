using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Server.Models;
using Server.Contracts;

namespace Server
{
    public class DatabaseHandler : IDatbaseHandler
    {
        SqlConnection connection;
        public DatabaseHandler()
        {
            connectToDatabase();
        }

        public void connectToDatabase()
        {
            try
            {
                string connetionString;
                connetionString = "Data Source=" + ApplicationConstants.serverName + ";Initial Catalog=" + ApplicationConstants.database + ";Persist Security Info=True;Trusted_Connection=True;MultipleActiveResultSets=true"";
                connection = new SqlConnection(connetionString);
                connection.Open();
                Console.WriteLine("Connected to Database");
            }
            catch
            {
                Console.WriteLine("Unable to Connect to Database");
            }
        }

       public bool verifyClientCredentials(string name, string role, string password)
        {
            string query = "";
            string nameInDB = "";
            if (role == ApplicationConstants.publisher)
            {
                query = "SELECT Name FROM  [dbo].[Publishers] where Name= @name and Password = @password";
            }
            else
            {
                query = "SELECT Name FROM  [dbo].[Subscriber] where Name= @name and Password = @password";
            }
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@password", password);
            command.Parameters.AddWithValue("@name", name);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                nameInDB = ((string)reader["Name"]);
            }
            reader.Close();
            if (nameInDB == name)
                return true;
            else
                return false;
        }

        private int getTopicId(string topic)
        {
            string query = "SELECT TopicId from Topics WHERE TopicName = @topic";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@topic", topic);
            SqlDataReader reader = command.ExecuteReader();
            int topicId = 0;
            while (reader.Read())
            {
                topicId = ((int)reader["TopicId"]);
            }
            reader.Close();
            return topicId;
        }

        private int getSubscriberId(string name)
        {
            string query = "SELECT Id from Subscriber WHERE Name = @name";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", name);
            SqlDataReader reader = command.ExecuteReader();
            int subscriberId = 0;
            while (reader.Read())
            {
                subscriberId = ((int)reader["Id"]);
            }
            reader.Close();
            return subscriberId;
        }

        public List<string> getAllTopics()
        {
            string query = "SELECT * from Topics";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            List<string> topics = new List<string>();
            while (reader.Read())
            {
                topics.Add((string)reader["TopicName"]);
            }
            reader.Close();
            return topics;
        }

        public void savePublisherMessage(string topic, string message)
        {
            int topicId = getTopicId(topic);
            List<ExpiredMessage> messages = checkForExpiredMessages(topicId);
            storeExpiredMessages(messages);
            deleteExpiredMessagesFromMessages(topicId);
            DateTime time = DateTime.Now;
            string query = "INSERT INTO Messages (Message, TopicId, TimeStamp) VALUES (@message, @topicId, @time)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@message", message);
            command.Parameters.AddWithValue("@topicId", topicId);
            command.Parameters.AddWithValue("@time", time);
            command.ExecuteNonQuery();
        }

        private List<ExpiredMessage> checkForExpiredMessages(int topicId)
        {
            DateTime currentDate = DateTime.Now;
            string query = "SELECT Message, TopicId from Messages WHERE TopicId = @topicId and Messages.TimeStamp < DATEADD(dd,-7,GETDATE())";
            List<ExpiredMessage> messages = new List<ExpiredMessage>();
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@topicId", topicId);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                ExpiredMessage newMessage = new ExpiredMessage();
                newMessage.message = ((string)reader["Message"]);
                newMessage.topicId = ((int)reader["TopicId"]);
                messages.Add(newMessage);
            }
            reader.Close();
            return messages;
        }

        private void storeExpiredMessages(List<ExpiredMessage> messages)
        {
            foreach (ExpiredMessage message in messages)
            {
                string query = "INSERT INTO DeadLetterMessage (Message, TopicId) VALUES (@message, @topicId)";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@message", message.message);
                command.Parameters.AddWithValue("@topicId", message.topicId);
                command.ExecuteNonQuery();
            }
        }

        private void deleteExpiredMessagesFromMessages(int topicId)
        {
            string query = "DELETE FROM Messages WHERE TopicId = @topicId and Messages.TimeStamp < DATEADD(dd,-7,GETDATE())";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@TopicId", topicId);
            command.ExecuteNonQuery();
        }

        public List<string> getSubscribedTopics(string clientName)
        {
            int subscriberId = getSubscriberId(clientName);
            List<int> topicsIds = getTopicIdsFromSubscriberTopicMapping(subscriberId);
            List<string> topics = new List<string>();
            foreach (int topicId in topicsIds)
            {
                string query = "SELECT TopicName from Topics WHERE TopicId = @topicId";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@topicId", topicId);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    topics.Add((string)reader["TopicName"]);
                }
                reader.Close();
            }
            return topics;
        }

        public List<int> getTopicIdsFromSubscriberTopicMapping(int subscriberId)
        {
            string query = "SELECT TopicId from SubscriberTopicMap WHERE SubscriberId = @subscriberId";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@subscriberId", subscriberId);
            SqlDataReader reader = command.ExecuteReader();
            List<int> topicsIds = new List<int>();
            while (reader.Read())
            {
                topicsIds.Add((int)reader["TopicId"]);
            }
            reader.Close();
            return topicsIds;
        }

        public List<string> pullMessages(string topic, string clientName)
        {
            int topicId = getTopicId(topic);
            string query = "SELECT message from Messages WHERE TopicId = @topicId";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@topicId", topicId);
            SqlDataReader reader = command.ExecuteReader();
            List<string> messages = new List<string>();
            while (reader.Read())
            {
                messages.Add((string)reader["Message"]);
            }
            reader.Close();
            return messages;
        }

        public void subscribeToTopic(string clientName, string topicName)
        {
            int topicId = getTopicId(topicName);
            int subscriberId = getSubscriberId(clientName);
            string query = "INSERT INTO SubscriberTopicMap (TopicId, SubscriberId) VALUES (@topicId, @subscriberId)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@topicId", topicId);
            command.Parameters.AddWithValue("@subscriberId", subscriberId);
            command.ExecuteNonQuery();
        }

        public void createTopic(string topicName)
        {
            string query = "INSERT INTO Topics (topicName) VALUES (@topicName)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@topicName", topicName);
            command.ExecuteNonQuery();
        }

        public void createUser(string clientName, string role, string password)
        {
            string query = "";
            if (role == "publisher")
            {
                query = "INSERT INTO Publishers (Name, Password) VALUES (@clientName,@password)";
            }
            else
            {
                query = "INSERT INTO Subscriber (Name, Password) VALUES (@clientName,@password)";
            }

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@clientName", clientName);
            command.Parameters.AddWithValue("@password", password);
            command.ExecuteNonQuery();
        }
    }
}
