using System;
using System.Net.Sockets;
using Server.Contracts;
using Client;
using IMQ.Protocol;

namespace Server.Controllers
{
    public class ClientRequestController : IClientRequestController
    {
        TcpClient client;
        LogHandler logger = new LogHandler();
        private IPublisherService publisherService;
        private ISubscriberService subscriberService;
        private IClientService clientService;
        NetworkHandler networkHandler;
        
        public ClientRequestController(IClientService clientService, IPublisherService publisherService, ISubscriberService subscriberService)
        {
            this.publisherService = publisherService;
            this.subscriberService = subscriberService;
            this.clientService = clientService;
            networkHandler = new NetworkHandler();
        }

        public void handleClientRequest(TcpClient client)
        {
            this.client = client;
            Request dataFromClient;
            networkHandler.createStreams(client);
            Console.WriteLine("Client now connected to server.");
            while (this.client.Connected)
            {
                try
                {
                    dataFromClient = recieveDataFromClient();
                    switch (dataFromClient.action)
                    {
                        case ApplicationConstants.getAllTopic:
                            this.clientService.sendAllTopics(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.publishMessage:
                            this.publisherService.publishMessage(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.getSubscribedTopic:
                            this.subscriberService.getSubscribedTopics(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.createTopic:
                            this.publisherService.createTopic(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.pullMessages:
                            this.subscriberService.pullMessages(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.subscribeToTopic:
                            this.subscriberService.subscribeTopic(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.register:
                            this.clientService.createUser(networkHandler, dataFromClient);
                            break;
                        case ApplicationConstants.login:
                            this.clientService.authenticateClient(networkHandler, dataFromClient); 
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Client is disconnected from Server");
                    logger.Error("Client is disconnected from Server");
                }
            }
        }

        public Request recieveDataFromClient()
        {
            Request dataFromClient = networkHandler.read();
            return dataFromClient;
        }
    }
}
