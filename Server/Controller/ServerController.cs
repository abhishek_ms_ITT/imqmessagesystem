using Server.Contracts;

namespace Server.Controllers
{
    class ServerController : IServerController
    {
        private IServerService serverService;

        public ServerController(IServerService serverService)
        {
            this.serverService = serverService;
        }

        public void startServer()
        {
            this.serverService.startServer();
        }
    }
}