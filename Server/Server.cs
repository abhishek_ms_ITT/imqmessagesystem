﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Server.Controllers;

namespace Server
{
    class IMQServer
    {
        static void Main(string[] args)
        {    
            var builder = new ConfigurationBuilder();

            var host = Startup.CreateHostBuilder(args).Build();

            var imqServer = ActivatorUtilities.CreateInstance<ServerController>(host.Services);
            imqServer.startServer();
        }
    }
}
