namespace Server
{
    public class ApplicationConstants { 
        public const string serverName  ="ITT-ABHISHEK-MS";
        public const string database = "IMQ2";
        public const string ipAddress  = "127.0.0.1";
        public const int portNumber = 8888;
        public const string publisher = "publisher";
        public const string subsciber = "subscriber";
        public const string publishMessage = "publishMessage";
        public const string getSubscribedTopic = "getSubscribedTopic";
        public const string createTopic = "createTopic";
        public const string pullMessages = "pullMessages";
        public const string subscribeToTopic = "subscribeToTopic";
        public const string register = "register";
        public const string login = "login";
        public const string getAllTopic = "getAllTopics";
        public const string IMQHeader = "IMQ";
    }
}