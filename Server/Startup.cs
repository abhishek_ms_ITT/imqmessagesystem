using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Server.Contracts;
using Server.Services;
using Server.Controllers;

namespace Server
{
    public class Startup
    {
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureServices((_, services) =>
            {
                services.AddScoped<IClientService, ClientService>();
                services.AddScoped<IServerService, ServerService>();
                services.AddScoped<IPublisherService, PublisherService>();
                services.AddScoped<ISubscriberService, SubscriberService>();
                services.AddScoped<IClientRequestController, ClientRequestController>();
                services.AddScoped<IServerController, ServerController>();
                services.AddScoped<IDatbaseHandler, DatabaseHandler>();
            });
    }
}