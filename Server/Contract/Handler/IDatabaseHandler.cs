using System.Collections.Generic;

namespace Server.Contracts
{
    public interface IDatbaseHandler
    {
        bool verifyClientCredentials(string name, string role, string password);
        List<string> getAllTopics();
        void savePublisherMessage(string topic, string message);
        List<string> getSubscribedTopics(string clientName);
        List<string> pullMessages(string topic, string clientName);
        void subscribeToTopic(string clientName, string topicName);
        void createTopic(string topicName);
        void createUser(string clientName, string role, string password);
    }
}