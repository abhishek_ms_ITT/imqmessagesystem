
using System.Net.Sockets;

namespace Server.Contracts
{
    public interface IClientRequestController
    {
        void handleClientRequest(TcpClient client);
    }
}