using IMQ.Protocol;

namespace Server.Contracts
{
    public interface IClientService
    {
        void sendAllTopics(NetworkHandler networkHandler, Request dataFromClient);
        void createUser(NetworkHandler networkHandler, Request dataFromClient);
        void authenticateClient(NetworkHandler networkHandler, Request dataFromClient);
    }
}