using IMQ.Protocol;

namespace Server.Contracts
{
    public interface ISubscriberService
    {
        void subscribeTopic(NetworkHandler networkHandler, Request request);

        void getSubscribedTopics(NetworkHandler networkHandler, Request request);

        void pullMessages(NetworkHandler networkHandler, Request request);
    }
}