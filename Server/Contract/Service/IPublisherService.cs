using IMQ.Protocol;

namespace Server.Contracts
{
    public interface IPublisherService
    {
        void publishMessage(NetworkHandler networkHandler, Request request);
        void createTopic(NetworkHandler networkHandler, Request request);
    }
}