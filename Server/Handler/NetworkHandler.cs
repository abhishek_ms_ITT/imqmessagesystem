using System;
using System.IO;
using System.Net.Sockets;
using IMQ.Protocol;
using Newtonsoft.Json;
using IMQ.Exceptions;

namespace Server
{
    public class NetworkHandler
    {
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;

        public void createStreams(TcpClient client)
        {
            stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }

        public void closeConnection()
        {
            stream.Close();
            streamReader.Close();
            streamWriter.Close();
        }

        public Request read()
        {
            Request request = JsonConvert.DeserializeObject<Request>(streamReader.ReadLine());
            if (request.header == ApplicationConstants.IMQHeader)
            {
                return request;
            }
            throw new Exception("Invalid Request");
        }

        public void write(Response response)
        {
            try
            {
                streamWriter.WriteLine(JsonConvert.SerializeObject(response));
                streamWriter.Flush();
            }
            catch
            {
                throw new NetworkStreamException("There is some problem with the server. Please try again later");
            }
        }
    }
}