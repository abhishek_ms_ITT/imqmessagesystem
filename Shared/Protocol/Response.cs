using System.Collections.Generic;

namespace IMQ.Protocol
{
    public class Response : IMQProtocol
    {
        public string responseMessage { get; set; }
        public List<string> data { get; set; }

    }
}
