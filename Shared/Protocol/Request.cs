namespace IMQ.Protocol
{
    public class Request : IMQProtocol
    {
        public string topic { get; set; }
        public string message { get; set; }
        public string clientName { get; set; }
        public string action { get; set; }
        public string role { get; set; }
        public string password { get; set; }
    }
}
