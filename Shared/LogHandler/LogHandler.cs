using Serilog;

namespace Client
{
    class LogHandler
    {
        ILogger logger;
        public LogHandler()
        {
            logger = new LoggerConfiguration()
                .WriteTo.File("Log/logs.txt")
                .WriteTo.Console()
                .CreateLogger();
        }

        public void Info(string message)
        {
            logger.Information(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }
    }
        
}