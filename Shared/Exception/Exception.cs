using System;

namespace IMQ.Exceptions
{
    [Serializable]
    class NetworkStreamException : Exception
    {
        public NetworkStreamException(string message) : base(String.Format(message)) { }
    }

    [Serializable]
    class ServerStartException : Exception
    {
        public ServerStartException(string message) : base(String.Format(message)) { }
    }
}